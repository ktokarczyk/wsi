﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Filters;


namespace WSI_projekt
{
    /* 
        potrzebne jest 17 kolumn dla datatable
        wyciągać z rowa konkretne cyferki  

     */



    public partial class Form1 : Form
    {
        // nie istotne 
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HTCAPTION = 0x2;
        [DllImport("User32.dll")]
        public static extern bool ReleaseCapture();
        [DllImport("User32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );
        // <-- nie istotne 


        private List<string> listA = new List<string>(); // lista pobierająca wszystkie dane z svc 
        private DataTable dataTable = new DataTable();
        private List<double> winList = new List<double>();

        public Form1()
        {
            InitializeComponent();
        }

        private void close_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            if (LoadData())
            {
                InitPlaceData();
                ToDataTable();
            }


        }

        private void InitPlaceData()
        {
           // dataTable.Columns.Add("Win", typeof(double));
            dataTable.Columns.Add("Rank", typeof(double));
            dataTable.Columns.Add("GMode", typeof(double));
            dataTable.Columns.Add("GType", typeof(double));
            dataTable.Columns.Add("H1", typeof(double));
            dataTable.Columns.Add("H2", typeof(double));
            dataTable.Columns.Add("H3", typeof(double));
            dataTable.Columns.Add("H4", typeof(double));
            dataTable.Columns.Add("H5", typeof(double));
            dataTable.Columns.Add("H6", typeof(double));
            dataTable.Columns.Add("H7", typeof(double));
            dataTable.Columns.Add("H8", typeof(double));
            dataTable.Columns.Add("H9", typeof(double));
            dataTable.Columns.Add("H10", typeof(double));
            dataTable.Columns.Add("H11", typeof(double));
            dataTable.Columns.Add("H12", typeof(double));
            dataTable.Columns.Add("H13", typeof(double));
            dataTable.Columns.Add("H14", typeof(double));
            dataTable.Columns.Add("H15", typeof(double));
            dataTable.Columns.Add("H16", typeof(double));
            dataTable.Columns.Add("H17", typeof(double));
            dataTable.Columns.Add("H18", typeof(double));
            dataTable.Columns.Add("H19", typeof(double));
            dataTable.Columns.Add("H20", typeof(double));
            dataTable.Columns.Add("H21", typeof(double));
            dataTable.Columns.Add("H22", typeof(double));
            dataTable.Columns.Add("H23", typeof(double));
            dataTable.Columns.Add("H24", typeof(double));
            dataTable.Columns.Add("H25", typeof(double));
            dataTable.Columns.Add("H26", typeof(double));
            dataTable.Columns.Add("H27", typeof(double));
            dataTable.Columns.Add("H28", typeof(double));
            dataTable.Columns.Add("H29", typeof(double));
            dataTable.Columns.Add("H30", typeof(double));
            dataTable.Columns.Add("H31", typeof(double));
            dataTable.Columns.Add("H32", typeof(double));
            dataTable.Columns.Add("H33", typeof(double));
            dataTable.Columns.Add("H34", typeof(double));
            dataTable.Columns.Add("H35", typeof(double));
            dataTable.Columns.Add("H36", typeof(double));
            dataTable.Columns.Add("H37", typeof(double));
            dataTable.Columns.Add("H38", typeof(double));
            dataTable.Columns.Add("H39", typeof(double));
            dataTable.Columns.Add("H40", typeof(double));
            dataTable.Columns.Add("H41", typeof(double));
            dataTable.Columns.Add("H42", typeof(double));
            dataTable.Columns.Add("H43", typeof(double));
            dataTable.Columns.Add("H44", typeof(double));
            dataTable.Columns.Add("H45", typeof(double));
            dataTable.Columns.Add("H46", typeof(double));
            dataTable.Columns.Add("H47", typeof(double));
            dataTable.Columns.Add("H48", typeof(double));
            dataTable.Columns.Add("H49", typeof(double));
            dataTable.Columns.Add("H50", typeof(double));
            dataTable.Columns.Add("H51", typeof(double));
            dataTable.Columns.Add("H52", typeof(double));
            dataTable.Columns.Add("H53", typeof(double));
            dataTable.Columns.Add("H54", typeof(double));
            dataTable.Columns.Add("H55", typeof(double));
            dataTable.Columns.Add("H56", typeof(double));
            dataTable.Columns.Add("H57", typeof(double));
            dataTable.Columns.Add("H58", typeof(double));
            dataTable.Columns.Add("H59", typeof(double));
            dataTable.Columns.Add("H60", typeof(double));
            dataTable.Columns.Add("H61", typeof(double));
            dataTable.Columns.Add("H62", typeof(double));
            dataTable.Columns.Add("H63", typeof(double));
            dataTable.Columns.Add("H64", typeof(double));
            dataTable.Columns.Add("H65", typeof(double));
            dataTable.Columns.Add("H66", typeof(double));
            dataTable.Columns.Add("H67", typeof(double));
            dataTable.Columns.Add("H68", typeof(double));
            dataTable.Columns.Add("H69", typeof(double));
            dataTable.Columns.Add("H70", typeof(double));
            dataTable.Columns.Add("H71", typeof(double));
            dataTable.Columns.Add("H72", typeof(double));
            dataTable.Columns.Add("H73", typeof(double));
            dataTable.Columns.Add("H74", typeof(double));
            dataTable.Columns.Add("H75", typeof(double));
            dataTable.Columns.Add("H76", typeof(double));
            dataTable.Columns.Add("H77", typeof(double));
            dataTable.Columns.Add("H78", typeof(double));
            dataTable.Columns.Add("H79", typeof(double));
            dataTable.Columns.Add("H80", typeof(double));
            dataTable.Columns.Add("H81", typeof(double));
            dataTable.Columns.Add("H82", typeof(double));
            dataTable.Columns.Add("H83", typeof(double));
            dataTable.Columns.Add("H84", typeof(double));
            dataTable.Columns.Add("H85", typeof(double));
            dataTable.Columns.Add("H86", typeof(double));
            dataTable.Columns.Add("H87", typeof(double));
            dataTable.Columns.Add("H88", typeof(double));
            dataTable.Columns.Add("H89", typeof(double));
            dataTable.Columns.Add("H90", typeof(double));
            dataTable.Columns.Add("H91", typeof(double));
            dataTable.Columns.Add("H92", typeof(double));
            dataTable.Columns.Add("H93", typeof(double));
            dataTable.Columns.Add("H94", typeof(double));
            dataTable.Columns.Add("H95", typeof(double));
            dataTable.Columns.Add("H96", typeof(double));
            dataTable.Columns.Add("H97", typeof(double));
            dataTable.Columns.Add("H98", typeof(double));
            dataTable.Columns.Add("H99", typeof(double));
            dataTable.Columns.Add("H100", typeof(double));
            dataTable.Columns.Add("H101", typeof(double));
            dataTable.Columns.Add("H102", typeof(double));
            dataTable.Columns.Add("H103", typeof(double));
            dataTable.Columns.Add("H104", typeof(double));
            dataTable.Columns.Add("H105", typeof(double));
            dataTable.Columns.Add("H106", typeof(double));
            dataTable.Columns.Add("H107", typeof(double));
            dataTable.Columns.Add("H108", typeof(double));
            dataTable.Columns.Add("H109", typeof(double));
            dataTable.Columns.Add("H110", typeof(double));
            dataTable.Columns.Add("H111", typeof(double));
            dataTable.Columns.Add("H112", typeof(double));
            dataTable.Columns.Add("H113", typeof(double));
        }

        private bool LoadData()
        {
            bool bResult = true;
            try
            {
                using (var reader = new StreamReader(@"C:\\Users\\mandaryn\\Desktop\\WSI\\dota2Train.csv"))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        listA.Add(line);
                    }
                }
                
            }
            catch (Exception ex)
            {
                bResult = false;
                MessageBox.Show(ex.Message);
            }
            return bResult;
        }



        private void ToDataTable()
        {

            double maxnumb = new double();
            double minnumb = new double();

            for (int i = 0; i < listA.Count; i++)
            {
                var line = listA[i].Split(',');
                int licz = 0;
                double[] line2 = new double[117];
                foreach (string elements in line)
                {
                    if (licz == 1)
                    {
                        maxnumb = Math.Max(maxnumb, Convert.ToDouble(line[licz]));
                        minnumb = Math.Min(minnumb, Convert.ToDouble(line[licz]));
                    }
                }
            }
                

            for (int i = 0; i < listA.Count; i++)
            {
                var line = listA[i].Split(',');
                int licz = 0;
                double[] line2 = new double[117];
                foreach (string elements in line)
                {
                    if (licz == 0)
                    {
                        var obj = Convert.ToDouble(line[licz]);
                        if (obj == -1)
                            winList.Add(0);
                        else
                            winList.Add(1);

                        licz++;
                    }
                    else if (licz == 1)
                    {                        
                        line2[licz - 1] = (Convert.ToDouble(line[licz]) - minnumb)/(maxnumb - minnumb);
                        licz++;
                    }
                    else if (licz == 2 || licz == 3)
                    {
                        line2[licz - 1] = Convert.ToDouble(line[licz]);
                        licz++;
                    }
                    else if (Convert.ToDouble(line[licz]) == -1)
                    {
                        line2[licz - 1] = 0;
                        licz++;
                    }
                    else if (Convert.ToDouble(line[licz]) == 0)
                    {
                        line2[licz - 1] = 0.5;
                        licz++;
                    }
                    else if (Convert.ToDouble(line[licz]) == 1)
                    {
                        line2[licz - 1] = 1;
                        licz++;
                    }
                    else
                    {
                        //i coś wywalające error
                        line2[licz - 1] = Convert.ToDouble(line[licz]);
                        licz++;
                    }

                }



                dataTable.Rows.Add(line2[0], line2[1], line2[2], line2[3], line2[4], line2[5], line2[6], line2[7], line2[8], line2[9], line2[10], line2[11], line2[12], line2[13], line2[14], line2[15], line2[16], line2[17], line2[18], line2[19], line2[20], line2[21], line2[22], line2[23], line2[24], line2[25], line2[26], line2[27], line2[28], line2[29], line2[30], line2[31], line2[32], line2[33], line2[34], line2[35], line2[36], line2[37], line2[38], line2[39], line2[40], line2[41], line2[42], line2[43], line2[44], line2[45], line2[46], line2[47], line2[48], line2[49], line2[50], line2[51], line2[52], line2[53], line2[54], line2[55], line2[56], line2[57], line2[58], line2[59], line2[60], line2[61], line2[62], line2[63], line2[64], line2[65], line2[66], line2[67], line2[68], line2[69], line2[70], line2[71], line2[72], line2[73], line2[74], line2[75], line2[76], line2[77], line2[78], line2[79], line2[80], line2[81], line2[82], line2[83], line2[84], line2[85], line2[86], line2[87], line2[88], line2[89], line2[90], line2[91], line2[92], line2[93], line2[94], line2[95], line2[96], line2[97], line2[98], line2[99], line2[100], line2[101], line2[102], line2[103], line2[104], line2[105], line2[106], line2[107], line2[108], line2[109], line2[110], line2[111], line2[112], line2[113], line2[114], line2[115]/*, line2[116]*/);   


            }

            foreach (DataColumn dc in dataTable.Columns)
            {
                dataGridView1.Columns.Add(new DataGridViewTextBoxColumn());
            }
            foreach (DataRow dr in dataTable.Rows)
            {
                dataGridView1.Rows.Add(dr.ItemArray);
            }


        }

        //private void Learning()
        //{
        //    // Now, we can create the sequential minimal optimization teacher
        //    var learn = new SequentialMinimalOptimization()
        //    {
        //        UseComplexityHeuristic = true,
        //        UseKernelEstimation = false
        //    };
                
        //    // And then we can obtain a trained SVM by calling its Learn method
        //    SupportVectorMachine svm = learn.Learn(dataTable,anserwe);
            
        //    // Finally, we can obtain the decisions predicted by the machine:
        //    bool[] prediction = svm.Decide(dataTable);
        //}

        private void panel_header_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            dataTable.Dispose();
        }
    }
}
